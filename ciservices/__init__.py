import pydantic_settings
import structlog

from .logconfig import configure_logging

LOG = structlog.get_logger()


class Settings(pydantic_settings.BaseSettings):
    verbose_logging: bool = True

    proxy_host: str = "0.0.0.0"
    proxy_port: int = 8080

    class Config:
        env_prefix = "auth_proxy_"


settings = Settings()


def main():
    configure_logging(verbose=settings.verbose_logging)
    # loop = asyncio.get_event_loop()
    # loop.run_until_complete(api_server.serve())
